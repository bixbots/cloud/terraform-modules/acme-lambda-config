# acme-lambda-config

## Summary

Uploads a YML configuration to the S3 bucket that is used by `acme-lambda` to generate Let's Encrypt certificates.

The lambda scans the S3 bucket daily for YML files and generates certificates for the configured domains. After the lambda executes, the S3 bucket will contain the public and private key pairs for the provisioned certificates.

More information about the free Let's Encrypt certificates is available [here](https://letsencrypt.org/).

## Resources

This module doesn't create any resources.

## Cost

Use of this module will incur fees depending on the storage consumed, access requests, and your lifecycle needs. Before using this module, please familiarize yourself with the expenses associated with S3.

* AWS S3 pricing information is available [here](https://aws.amazon.com/s3/pricing/)

## Inputs

| Name           | Description                                                                 | Type     | Default                  | Required |
|:---------------|:----------------------------------------------------------------------------|:---------|:-------------------------|:---------|
| lambda_name    | Specifies the name of the lambda that automates Let's Encrypt certificates. | `string` | `acme-management-lambda` | yes      |
| config_name    | Specifies the name of the configuration (without the `.yml` extension).     | `string` | -                        | yes      |
| config_content | Contains the content for YML configuration file.                            | `string` | -                        | yes      |

## Outputs

This module doesn't output anything.

## Examples

### Example YML

```yaml
directory: https://acme-staging-v02.api.letsencrypt.org/directory
email: someone@example.com
zone_name: example.cloud
reuse_key: true
renew_threshold_days: 30
domains:
  - subject: example.cloud
    alternate_names:
      - "*.example.cloud"
```

### Example Usage

```terraform
module "acme_lambda_config" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/acme-lambda-config.git?ref=v1"

  config_name    = "example"
  config_content = file("${path.module}/example.yml")
}
```

## Version History

* v1 - Initial Release
