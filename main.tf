locals {
  config_name = trimsuffix(var.config_name, ".yml")
}

resource "aws_s3_bucket_object" "config" {
  bucket = data.aws_s3_bucket.primary.id

  key          = "${local.config_name}/${local.config_name}.yml"
  content      = var.config_content
  content_type = "text/yaml"
  etag         = md5(var.config_content)

  server_side_encryption = "AES256"
}
