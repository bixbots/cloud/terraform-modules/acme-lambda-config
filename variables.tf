variable "lambda_name" {
  type        = string
  default     = "acme-management-lambda"
  description = "Specifies the name of the lambda that automates Let's Encrypt certificates."
}

variable "config_name" {
  type        = string
  description = "Specifies the name of the configuration (without the `.yml` extension)."
}

variable "config_content" {
  type        = string
  description = "Contains the content for YML configuration file."
}
